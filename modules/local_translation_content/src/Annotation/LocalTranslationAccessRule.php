<?php

namespace Drupal\local_translation_content\Annotation;

use Drupal\Component\Annotation\PluginID;

/**
 * Defines the annotation for access rules plugins.
 *
 * @Annotation
 */
class LocalTranslationAccessRule extends PluginID {

}
